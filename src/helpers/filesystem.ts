import { AlgorithmType, KeyFormatType } from '../types';
import { fromEllipticCurvePrivateKey } from './ellipticCurve/fromPrivateKey';
import { fromEllipticCurvePublicKey } from './ellipticCurve/fromPublicKey';
import { toEllipticCurvePrivateKey } from './ellipticCurve/toPrivateKey';
import { toEllipticCurvePublicKey } from './ellipticCurve/toPublicKey';
import { KeyObject } from 'crypto';
import { existsSync } from 'fs';
import { mkdir, readFile, writeFile } from 'fs/promises';
import { parse } from 'path';

export const saveFile = async (path: string, data: string) => {
  const { dir } = parse(path);

  if (!existsSync(dir)) {
    await mkdir(dir, { recursive: true });
  }

  return await writeFile(path, data);
};

export const loadPrivateKey = async (path: string, type: AlgorithmType, format: KeyFormatType) => {
  const value = (await readFile(path, 'utf8')).trim();
  return fromEllipticCurvePrivateKey(value, type, format);
};

export const savePrivateKey = async (path: string, value: KeyObject, type: AlgorithmType, format: KeyFormatType) => {
  const data = toEllipticCurvePrivateKey(value, type, format);
  return await saveFile(path, data);
};

export const loadPublicKey = async (path: string, type: AlgorithmType, format: KeyFormatType) => {
  const value = (await readFile(path, 'utf8')).trim();
  return fromEllipticCurvePublicKey(value, type, format);
};

export const savePublicKey = async (path: string, value: KeyObject, type: AlgorithmType, format: KeyFormatType) => {
  const data = toEllipticCurvePublicKey(value, type, format);
  return await saveFile(path, data);
};
