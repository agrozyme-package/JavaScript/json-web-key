import { privateKeyBytesList, publicKeyBytesList } from '../settings';
import { AlgorithmType, algorithmTypes, keyFormatTypes } from '../types';
import { isHexString } from '@ethersproject/bytes';

export const isAlgorithmType = (value: string) => {
  return (<string[]>[...algorithmTypes]).includes(value);
};

export const isKeyFomartType = (value: string) => {
  return (<string[]>[...keyFormatTypes]).includes(value);
};

export const isRawPrivateKey = (value: string, type: AlgorithmType) => {
  const bytes = privateKeyBytesList[type];
  return isHexString(value, bytes);
};

export const isRawPublicKey = (value: string, type: AlgorithmType) => {
  const bytes = publicKeyBytesList[type];
  const items = [bytes, bytes - 1, (bytes - 1) / 2 + 1];
  return items.some((item) => isHexString(value, item));
};
