import { AlgorithmType } from '../types';
import { isAlgorithmType, isKeyFomartType, isRawPrivateKey, isRawPublicKey } from './validator';

export const checkKeyFomartType = (value: string) => {
  if (!isKeyFomartType(value)) {
    throw Error(`value is not valid key format: ${value}`);
  }
};

export const checkAlgorithmType = (value: string) => {
  if (!isAlgorithmType(value)) {
    throw Error(`value is not valid algorithm: ${value}`);
  }
};

export const checkRawPrivateKey = (value: string, type: AlgorithmType) => {
  if (!isRawPrivateKey(value, type)) {
    throw Error(`value is not valid raw private key: ${value}`);
  }
};

export const checkRawPublicKey = (value: string, type: AlgorithmType) => {
  if (!isRawPublicKey(value, type)) {
    throw Error(`value is not valid raw public key: ${value}`);
  }
};
