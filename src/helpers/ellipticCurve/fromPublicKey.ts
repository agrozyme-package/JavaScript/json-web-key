import { AlgorithmType, FromKeyFunction, KeyFormatType } from '../../types';
import { getJwkFromRawPublicKey } from './keyPair';
import { createPublicKey } from 'crypto';

export const fromJwkPublicKey = (value: string, type: AlgorithmType) => {
  return createPublicKey({
    key: JSON.parse(value),
    format: 'jwk',
  });
};

export const fromRawPublicKey = (value: string, type: AlgorithmType) => {
  const jwk = getJwkFromRawPublicKey(value, type);
  return fromJwkPublicKey(JSON.stringify(jwk), type);
};

export const fromPemPublicKey = (value: string, type: AlgorithmType) => {
  return createPublicKey({
    key: value,
    format: 'pem',
    type: 'spki',
  });
};

export const fromEllipticCurvePublicKeyList: Record<KeyFormatType, FromKeyFunction> = {
  jwk: fromJwkPublicKey,
  pem: fromPemPublicKey,
  raw: fromRawPublicKey,
};

export const fromEllipticCurvePublicKey = (value: string, type: AlgorithmType, format: KeyFormatType) => {
  const invoke = fromEllipticCurvePublicKeyList[format];
  return invoke(value, type);
};
