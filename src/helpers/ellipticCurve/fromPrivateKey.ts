import { AlgorithmType, FromKeyFunction, KeyFormatType } from '../../types';
import { getJwkFromRawPrivateKey } from './keyPair';
import { createPrivateKey } from 'crypto';

export const fromJwkPrivateKey: FromKeyFunction = (value: string, type: AlgorithmType) => {
  return createPrivateKey({
    key: JSON.parse(value),
    format: 'jwk',
  });
};

export const fromRawPrivateKey: FromKeyFunction = (value: string, type: AlgorithmType) => {
  const jwk = getJwkFromRawPrivateKey(value, type);
  return fromJwkPrivateKey(JSON.stringify(jwk), type);
};

export const fromPemPrivateKey: FromKeyFunction = (value: string, type: AlgorithmType) => {
  return createPrivateKey({
    key: value,
    format: 'pem',
    type: 'pkcs8',
  });
};

export const fromEllipticCurvePrivateKeyList: Record<KeyFormatType, FromKeyFunction> = {
  jwk: fromJwkPrivateKey,
  pem: fromPemPrivateKey,
  raw: fromRawPrivateKey,
};

export const fromEllipticCurvePrivateKey = (value: string, type: AlgorithmType, format: KeyFormatType) => {
  const invoke = fromEllipticCurvePrivateKeyList[format];
  return invoke(value, type);
};
