import { algorithmCurveList, algorithmParameterList } from '../../settings';
import { AlgorithmType } from '../../types';
import { checkRawPrivateKey, checkRawPublicKey } from '../check';
import { arrayify } from '@ethersproject/bytes';
import { encodeURLSafe } from '@stablelib/base64';
import { createECDH, ECDH, JsonWebKey } from 'crypto';

export const makeRawKeyPair = (data: Uint8Array, type: AlgorithmType) => {
  const curve = algorithmCurveList[type];
  const ecdh = createECDH(curve);
  // ecdh.generateKeys();
  // console.dir({ size: ecdh.getPrivateKey().length });
  ecdh.setPrivateKey(data);
  const privateKey = arrayify(ecdh.getPrivateKey());
  const publicKey = arrayify(ecdh.getPublicKey());
  // console.dir({ privateKey: privateKey.length, publicKey: publicKey.length });
  return { privateKey, publicKey };
};

export const wrapJwk = (type: AlgorithmType, publicKey: Uint8Array, privateKey?: Uint8Array) => {
  const size = (publicKey.length - 1) / 2;
  const d = undefined === privateKey ? undefined : encodeURLSafe(privateKey);
  const x = encodeURLSafe(publicKey.slice(1, size + 1));
  const y = encodeURLSafe(publicKey.slice(size + 1));
  return Object.assign(<JsonWebKey>{}, algorithmParameterList[type], { d, x, y });
};

export const getJwkFromRawPrivateKey = (value: string, type: AlgorithmType) => {
  checkRawPrivateKey(value, type);
  const { privateKey, publicKey } = makeRawKeyPair(arrayify(value), type);
  return wrapJwk(type, publicKey, privateKey);
};

export const convertPublicKey = (data: Uint8Array, type: AlgorithmType, compressed: boolean = false) => {
  const curve = algorithmCurveList[type];
  const format = compressed ? 'compressed' : 'uncompressed';
  const publicKey = ECDH.convertKey(data, curve, undefined, undefined, format);
  return arrayify(publicKey);
};

export const getJwkFromRawPublicKey = (value: string, type: AlgorithmType) => {
  checkRawPublicKey(value, type);
  const data = arrayify(value);
  const publicKey = convertPublicKey(data, type, false);
  return wrapJwk(type, publicKey);
};
