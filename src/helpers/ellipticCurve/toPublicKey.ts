import { algorithmCurveList } from '../../settings';
import { AlgorithmType, KeyFormatType, ToKeyFunction } from '../../types';
import { convertPublicKey } from './keyPair';
import { concat, hexlify } from '@ethersproject/bytes';
import { decodeURLSafe } from '@stablelib/base64';
import { JsonWebKey, KeyObject } from 'crypto';

export const toJwkPublicKey = (value: KeyObject, type: AlgorithmType) => {
  const jwk = value.export({ format: 'jwk' });
  return JSON.stringify(jwk);
};

export const toRawPublicKey = (value: KeyObject, type: AlgorithmType) => {
  const data = toJwkPublicKey(value, type);
  const { x, y } = <JsonWebKey>JSON.parse(data);
  const key = concat([[0x04], decodeURLSafe(<string>x), decodeURLSafe(<string>y)]);
  const curve = algorithmCurveList[type];
  const publicKey = convertPublicKey(key, type, true);
  return hexlify(publicKey);
};

export const toPemPublicKey = (value: KeyObject, type: AlgorithmType) => {
  return <string>value.export({ format: 'pem', type: 'spki' });
};

export const toEllipticCurvePublicKeyList: Record<KeyFormatType, ToKeyFunction> = {
  jwk: toJwkPublicKey,
  pem: toPemPublicKey,
  raw: toRawPublicKey,
};

export const toEllipticCurvePublicKey = (value: KeyObject, type: AlgorithmType, format: KeyFormatType) => {
  const invoke = toEllipticCurvePublicKeyList[format];
  return invoke(value, type);
};
