import { AlgorithmType, KeyFormatType, ToKeyFunction } from '../../types';
import { hexlify } from '@ethersproject/bytes';
import { decodeURLSafe } from '@stablelib/base64';
import { JsonWebKey, KeyObject } from 'crypto';

export const toJwkPrivateKey: ToKeyFunction = (value: KeyObject, type: AlgorithmType) => {
  const jwk = value.export({ format: 'jwk' });
  return JSON.stringify(jwk);
};

export const toRawPrivateKey: ToKeyFunction = (value: KeyObject, type: AlgorithmType) => {
  const data = toJwkPrivateKey(value, type);
  const { d } = <JsonWebKey>JSON.parse(data);
  return undefined === d ? '' : hexlify(decodeURLSafe(d));
};

export const toPemPrivateKey: ToKeyFunction = (value: KeyObject, type: AlgorithmType) => {
  return <string>value.export({ format: 'pem', type: 'pkcs8' });
};

export const toEllipticCurvePrivateKeyList: Record<KeyFormatType, ToKeyFunction> = {
  jwk: toJwkPrivateKey,
  pem: toPemPrivateKey,
  raw: toRawPrivateKey,
};

export const toEllipticCurvePrivateKey = (value: KeyObject, type: AlgorithmType, format: KeyFormatType) => {
  const invoke = toEllipticCurvePrivateKeyList[format];
  return invoke(value, type);
};
