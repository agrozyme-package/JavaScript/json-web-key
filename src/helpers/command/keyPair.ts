import { AlgorithmType } from '../../types';
import { fromEllipticCurvePrivateKey } from '../ellipticCurve/fromPrivateKey';
import { fromEllipticCurvePublicKey } from '../ellipticCurve/fromPublicKey';
import { toJwkPrivateKey, toPemPrivateKey, toRawPrivateKey } from '../ellipticCurve/toPrivateKey';
import { toJwkPublicKey, toPemPublicKey, toRawPublicKey } from '../ellipticCurve/toPublicKey';

export const toAllFormatPrivateKey = (rawPrivateKey: string, type: AlgorithmType) => {
  const value = fromEllipticCurvePrivateKey(rawPrivateKey, type, 'raw');
  const raw = toRawPrivateKey(value, type);
  const jwk = JSON.parse(toJwkPrivateKey(value, type));
  const pem = toPemPrivateKey(value, type);
  return {
    raw,
    jwk,
    pem,
  };
};

export const toAllFormatPublicKey = (rawPublicKey: string, type: AlgorithmType) => {
  const value = fromEllipticCurvePublicKey(rawPublicKey, type, 'raw');
  const raw = toRawPublicKey(value, type);
  const jwk = JSON.parse(toJwkPublicKey(value, type));
  const pem = toPemPublicKey(value, type);
  return {
    raw,
    jwk,
    pem,
  };
};
