import { BaseCommand } from '../../class/BaseCommand';
import { checkAlgorithmType, checkKeyFomartType, checkRawPublicKey } from '../../helpers/check';
import { fromEllipticCurvePublicKey } from '../../helpers/ellipticCurve/fromPublicKey';
import { toEllipticCurvePublicKey } from '../../helpers/ellipticCurve/toPublicKey';
import { AlgorithmType, algorithmTypes, KeyFormatType, keyFormatTypes } from '../../types';
import { Flags } from '@oclif/core';

export default class PublicKeyConvert extends BaseCommand {
  static description = 'convert raw public key to other format';

  static flags = {
    ...BaseCommand.flags,
    key: Flags.string({ required: true }),
    format: Flags.string({ required: true, options: [...keyFormatTypes] }),
    type: Flags.string({ required: true, options: [...algorithmTypes] }),
  };

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(PublicKeyConvert);
    const { key } = flags;
    const type = <AlgorithmType>flags.type;
    const format = <KeyFormatType>flags.format;

    checkKeyFomartType(format);
    checkAlgorithmType(type);
    checkRawPublicKey(key, type);

    const value = fromEllipticCurvePublicKey(key, type, 'raw');
    const data = toEllipticCurvePublicKey(value, type, format);
    const result = 'jwk' === format ? JSON.parse(data) : data;
    console.dir(result);
  }
}
