import { BaseCommand } from '../../class/BaseCommand';
import { checkAlgorithmType, checkKeyFomartType, checkRawPrivateKey } from '../../helpers/check';
import { fromEllipticCurvePrivateKey } from '../../helpers/ellipticCurve/fromPrivateKey';
import { toEllipticCurvePrivateKey } from '../../helpers/ellipticCurve/toPrivateKey';
import { AlgorithmType, algorithmTypes, KeyFormatType, keyFormatTypes } from '../../types';
import { Flags } from '@oclif/core';

export default class PrivateKeyConvert extends BaseCommand {
  static description = 'convert raw private key to other format';

  static flags = {
    ...BaseCommand.flags,
    key: Flags.string({ required: true }),
    format: Flags.string({ required: true, options: [...keyFormatTypes] }),
    type: Flags.string({ required: true, options: [...algorithmTypes] }),
  };

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(PrivateKeyConvert);

    const { key } = flags;
    const type = <AlgorithmType>flags.type;
    const format = <KeyFormatType>flags.format;

    checkKeyFomartType(format);
    checkAlgorithmType(type);
    checkRawPrivateKey(key, type);

    const value = fromEllipticCurvePrivateKey(key, type, 'raw');
    const data = toEllipticCurvePrivateKey(value, type, format);
    const result = 'jwk' === format ? JSON.parse(data) : data;
    console.dir(result);
  }
}
