import { BaseCommand } from '../../class/BaseCommand';
import { checkAlgorithmType, checkKeyFomartType } from '../../helpers/check';
import { loadPrivateKey, savePrivateKey } from '../../helpers/filesystem';
import { AlgorithmType, algorithmTypes, KeyFormatType, keyFormatTypes } from '../../types';
import { Flags } from '@oclif/core';
import consola from 'consola';

export default class PrivateKeyExport extends BaseCommand {
  static description = 'export private key file to other format';

  static flags = {
    ...BaseCommand.flags,
    fromFile: Flags.file({ required: true, exists: true }),
    toFile: Flags.file({ required: true }),
    fromFormat: Flags.string({ required: true, options: [...keyFormatTypes] }),
    toFormat: Flags.string({ required: true, options: [...keyFormatTypes] }),
    type: Flags.string({ required: true, options: [...algorithmTypes] }),
  };

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(PrivateKeyExport);
    const { fromFile, toFile } = flags;
    const type = <AlgorithmType>flags.type;
    const fromFormat = <KeyFormatType>flags.fromFormat;
    const toFormat = <KeyFormatType>flags.toFormat;

    checkKeyFomartType(fromFormat);
    checkKeyFomartType(toFormat);
    checkAlgorithmType(type);

    const value = await loadPrivateKey(<string>fromFile, type, fromFormat);
    await savePrivateKey(<string>toFile, value, type, toFormat);
    consola.success(`save file: ${toFile}`);
  }
}
