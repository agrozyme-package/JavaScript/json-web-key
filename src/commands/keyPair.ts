import { BaseCommand } from '../class/BaseCommand';
import { checkAlgorithmType } from '../helpers/check';
import { toAllFormatPrivateKey, toAllFormatPublicKey } from '../helpers/command/keyPair';
import { makeRawKeyPair } from '../helpers/ellipticCurve/keyPair';
import { privateKeyBytesList } from '../settings';
import { AlgorithmType, algorithmTypes } from '../types';
import { arrayify, hexlify } from '@ethersproject/bytes';
import { Flags } from '@oclif/core';
import { randomBytes } from 'crypto';
import { InspectOptions } from 'util';

export default class KeyPair extends BaseCommand {
  static description = 'make keypair';

  static flags = { ...BaseCommand.flags, type: Flags.string({ required: true, options: [...algorithmTypes] }) };

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(KeyPair);
    const type = <AlgorithmType>flags.type;
    checkAlgorithmType(type);

    const options: InspectOptions = {
      colors: true,
      customInspect: true,
      compact: false,
      breakLength: Number.POSITIVE_INFINITY,
    };

    const data = arrayify(randomBytes(privateKeyBytesList[type]));
    const keyPair = makeRawKeyPair(data, type);

    const privateKey = toAllFormatPrivateKey(hexlify(keyPair.privateKey), type);
    console.log(`privateKey:`);
    console.dir(privateKey, options);

    const publicKey = toAllFormatPublicKey(hexlify(keyPair.publicKey), type);
    console.log(`publicKey:`);
    console.dir(publicKey, options);
  }
}
