import { AlgorithmType, EllipticCurveParameter, WebCryptoAlgorithm } from './types';

export const algorithmList: Record<AlgorithmType, WebCryptoAlgorithm> = {
  // RS256: '',
  // RS384: '',
  // RS512: '',
  // PS256: '',
  // PS384: '',
  // PS512: '',
  // 'RSA-OAEP': '',
  // 'RSA-OAEP-256': '',
  // 'RSA-OAEP-384': '',
  // 'RSA-OAEP-512': '',
  // RSA1_5: '',
  ES256: { name: 'ECDSA', namedCurve: 'P-256' },
  ES256K: { name: 'ECDSA', namedCurve: 'secp256k1' },
  ES384: { name: 'ECDSA', namedCurve: 'P-384' },
  ES512: { name: 'ECDSA', namedCurve: 'P-521' },
  // EdDSA: '',
  // 'ECDH-ES': '',
  // 'ECDH-ES+A128KW': '',
  // 'ECDH-ES+A192KW': '',
  // 'ECDH-ES+A256KW': '',
};

export const algorithmParameterList: Record<AlgorithmType, EllipticCurveParameter> = {
  ES256: { kty: 'EC', crv: 'P-256' },
  ES256K: { kty: 'EC', crv: 'secp256k1' },
  ES384: { kty: 'EC', crv: 'P-384' },
  ES512: { kty: 'EC', crv: 'P-521' },
};

export const algorithmCurveList: Record<AlgorithmType, string> = {
  ES256: 'prime256v1',
  ES256K: 'secp256k1',
  ES384: 'secp384r1',
  ES512: 'secp521r1',
};

export const privateKeyBytesList: Record<AlgorithmType, number> = {
  ES256: 32,
  ES256K: 32,
  ES384: 48,
  ES512: 65,
};

export const publicKeyBytesList: Record<AlgorithmType, number> = {
  ES256: 65,
  ES256K: 65,
  ES384: 97,
  ES512: 133,
};
