import { JsonWebKey, KeyObject } from 'crypto';

// export const algorithmTypes = <const>[
//   'RS256',
//   'RS384',
//   'RS512',
//   'PS256',
//   'PS384',
//   'PS512',
//   'RSA-OAEP',
//   'RSA-OAEP-256',
//   'RSA-OAEP-384',
//   'RSA-OAEP-512',
//   'RSA1_5',
//   'ES256',
//   'ES256K',
//   'ES384',
//   'ES512',
//   'EdDSA',
//   'ECDH-ES',
//   'ECDH-ES+A128KW',
//   'ECDH-ES+A192KW',
//   'ECDH-ES+A256KW',
// ];

export const rsaTypes = <const>['RS256', 'RS384', 'RS512'];
export type RsaType = typeof rsaTypes[number];

export const ellipticCurveTypes = <const>['ES256', 'ES256K', 'ES384', 'ES512'];
export type EllipticCurveType = typeof ellipticCurveTypes[number];

// export const algorithmTypes = <const>[...rsaTypes, ...ellipticCurveTypes];
// export type AlgorithmType = RsaType | EllipticCurveType;

export const algorithmTypes = <const>[...ellipticCurveTypes];
export type AlgorithmType = EllipticCurveType;

export interface EllipticCurveParameter extends Required<Pick<JsonWebKey, 'kty' | 'crv'>> {}
export interface RsaParameter extends Required<Pick<JsonWebKey, 'kty' | 'n' | 'e'>> {}
export type AlgorithmParameter = RsaParameter | EllipticCurveParameter;

export interface AlgorithmParameterList extends Record<AlgorithmType, AlgorithmParameter> {}

export type WebCryptoAlgorithm =
  | AlgorithmIdentifier
  | RsaHashedImportParams
  | EcKeyImportParams
  | HmacImportParams
  | AesKeyAlgorithm;

export const keyFormatTypes = <const>['raw', 'jwk', 'pem'];
export type KeyFormatType = typeof keyFormatTypes[number];

export type FromKeyFunction = (value: string, type: AlgorithmType) => KeyObject;
export type ToKeyFunction = (value: KeyObject, type: AlgorithmType) => string;
