import { Command, Flags } from '@oclif/core';
import consola from 'consola';

export abstract class BaseCommand extends Command {
  static flags = {
    debug: Flags.boolean({ default: false, description: `show error stack` }),
  };

  async catch(error: Error & { exitCode?: number }) {
    const { flags } = await this.parse(<typeof BaseCommand>this.constructor);

    if (flags.debug) {
      throw error;
    }

    consola.error(error.message);
  }
}
