Json Web Key
=================

Convert key format for JWK
Support algorithm: ES256, ES256K, ES384, ES512
Support key format: raw, pem, jwk

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/@agrozyme/json-web-key)](https://npmjs.org/package/@agrozyme/json-web-key)
[![Downloads/week](https://img.shields.io/npm/dw/@agrozyme/json-web-key.svg)](https://npmjs.org/package/@agrozyme/json-web-key)
[![License](https://img.shields.io/npm/l/@agrozyme/json-web-key.svg)](https://gitlab.com/agrozyme-package/JavaScript/json-web-key/-/blob/main/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g @agrozyme/json-web-key
$ json-web-key COMMAND
running command...
$ json-web-key (--version)
@agrozyme/json-web-key/0.0.0 win32-x64 node-v16.14.2
$ json-web-key --help [COMMAND]
USAGE
  $ json-web-key COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`json-web-key help [COMMAND]`](#json-web-key-help-command)
* [`json-web-key keyPair`](#json-web-key-keypair)
* [`json-web-key privateKey:convert`](#json-web-key-privatekeyconvert)
* [`json-web-key privateKey:export`](#json-web-key-privatekeyexport)
* [`json-web-key publicKey:convert`](#json-web-key-publickeyconvert)
* [`json-web-key publicKey:export`](#json-web-key-publickeyexport)

## `json-web-key help [COMMAND]`

Display help for json-web-key.

```
USAGE
  $ json-web-key help [COMMAND] [-n]

ARGUMENTS
  COMMAND  Command to show help for.

FLAGS
  -n, --nested-commands  Include all nested commands in the output.

DESCRIPTION
  Display help for json-web-key.
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v5.1.12/src/commands/help.ts)_

## `json-web-key keyPair`

make keypair

```
USAGE
  $ json-web-key keyPair --type ES256|ES256K|ES384|ES512 [--debug]

FLAGS
  --debug          show error stack
  --type=<option>  (required)
                   <options: ES256|ES256K|ES384|ES512>

DESCRIPTION
  make keypair
```

_See code: [dist/commands/keyPair.ts](https://gitlab.com/agrozyme-package/JavaScript/json-web-key/blob/v0.0.0/dist/commands/keyPair.ts)_

## `json-web-key privateKey:convert`

convert raw private key to other format

```
USAGE
  $ json-web-key privateKey:convert --key <value> --format raw|jwk|pem --type ES256|ES256K|ES384|ES512 [--debug]

FLAGS
  --debug            show error stack
  --format=<option>  (required)
                     <options: raw|jwk|pem>
  --key=<value>      (required)
  --type=<option>    (required)
                     <options: ES256|ES256K|ES384|ES512>

DESCRIPTION
  convert raw private key to other format
```

_See code: [dist/commands/privateKey/convert.ts](https://gitlab.com/agrozyme-package/JavaScript/json-web-key/blob/v0.0.0/dist/commands/privateKey/convert.ts)_

## `json-web-key privateKey:export`

export private key file to other format

```
USAGE
  $ json-web-key privateKey:export --fromFile <value> --toFile <value> --fromFormat raw|jwk|pem --toFormat raw|jwk|pem
    --type ES256|ES256K|ES384|ES512 [--debug]

FLAGS
  --debug                show error stack
  --fromFile=<value>     (required)
  --fromFormat=<option>  (required)
                         <options: raw|jwk|pem>
  --toFile=<value>       (required)
  --toFormat=<option>    (required)
                         <options: raw|jwk|pem>
  --type=<option>        (required)
                         <options: ES256|ES256K|ES384|ES512>

DESCRIPTION
  export private key file to other format
```

_See code: [dist/commands/privateKey/export.ts](https://gitlab.com/agrozyme-package/JavaScript/json-web-key/blob/v0.0.0/dist/commands/privateKey/export.ts)_

## `json-web-key publicKey:convert`

convert raw public key to other format

```
USAGE
  $ json-web-key publicKey:convert --key <value> --format raw|jwk|pem --type ES256|ES256K|ES384|ES512 [--debug]

FLAGS
  --debug            show error stack
  --format=<option>  (required)
                     <options: raw|jwk|pem>
  --key=<value>      (required)
  --type=<option>    (required)
                     <options: ES256|ES256K|ES384|ES512>

DESCRIPTION
  convert raw public key to other format
```

_See code: [dist/commands/publicKey/convert.ts](https://gitlab.com/agrozyme-package/JavaScript/json-web-key/blob/v0.0.0/dist/commands/publicKey/convert.ts)_

## `json-web-key publicKey:export`

export public key file to other format

```
USAGE
  $ json-web-key publicKey:export --fromFile <value> --toFile <value> --fromFormat raw|jwk|pem --toFormat raw|jwk|pem
    --type ES256|ES256K|ES384|ES512 [--debug]

FLAGS
  --debug                show error stack
  --fromFile=<value>     (required)
  --fromFormat=<option>  (required)
                         <options: raw|jwk|pem>
  --toFile=<value>       (required)
  --toFormat=<option>    (required)
                         <options: raw|jwk|pem>
  --type=<option>        (required)
                         <options: ES256|ES256K|ES384|ES512>

DESCRIPTION
  export public key file to other format
```

_See code: [dist/commands/publicKey/export.ts](https://gitlab.com/agrozyme-package/JavaScript/json-web-key/blob/v0.0.0/dist/commands/publicKey/export.ts)_
<!-- commandsstop -->
